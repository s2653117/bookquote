package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    public double getBookPrice(String isbn) {
        Map<String, Double> dumbImplementation = new HashMap<>();
        dumbImplementation.put("1", 10.0);
        dumbImplementation.put("2", 45.0);
        dumbImplementation.put("3", 20.0);
        dumbImplementation.put("4", 35.0);
        dumbImplementation.put("5", 50.0);

        return dumbImplementation.get(isbn);
    }
}